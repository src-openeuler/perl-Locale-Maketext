%define mod_name Locale-Maketext

Name:         perl-Locale-Maketext
Epoch:        1
Version:      1.33
Release:      2
Summary:      Framework for localization
License:      GPL-1.0-or-later OR Artistic-1.0-Perl
URL:          https://metacpan.org/release/%{mod_name}
Source0:      https://cpan.metacpan.org/authors/id/T/TO/TODDR/%{mod_name}-%{version}.tar.gz
BuildArch:    noarch

BuildRequires:perl-interpreter perl-generators perl(ExtUtils::MakeMaker) perl(Test::More)
Requires:     perl(warnings)

%description
Locale::Maketext is a framework for software localization; it provides you with the tools
for organizing and accessing the bits of text and text-processing code that you need for
producing localized applications.

%package_help

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%doc ChangeLog README
%{perl_vendorlib}/*

%files help
%{_mandir}/*/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1:1.33-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Jan 25 2024 zhangyao <zhangyao108@huawei.com> - 1:1.33-1
- upgrade version to 1.33


* Mon Aug 7 2023 zhangyao <zhangyao108@huawei.com> - 1:1.32-1
- upgrade version to 1.32

* Mon Oct 31 2022 hongjinghao <hongjinghao@huawei.com> - 1:1.29-2
- use %{mod_name} marco

* Sat Jul 25 2020 zhanzhimin <zhanzhimin@huawei.com> - 1.29-1
- Update to 1.29 as provided in perl-5.28.0-RC1

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:1.28-1
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove unnecessary files

* Sat Sep 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.29-420
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: remove some notes

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.29-419
- Package init
